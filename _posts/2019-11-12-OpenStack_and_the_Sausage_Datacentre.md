---
title: OpenStack and the Sausage Datacentre
author: Tai Kedzierski
date: 2019-11-12 23:54:08
layout: post
---

Nick Jones is offering a talk on the Sausage Data Centre - spin up tiny weiners with 512MB RAM and 1GB storage, up to massive cumberlands with 32GB RAM and 100GB disk and 8 CPUs...! Run out of a datacentre in the back-end of Scotland, the talk will cover how the datancentre is run, and how open stack is leveraged to its fullest potential!
