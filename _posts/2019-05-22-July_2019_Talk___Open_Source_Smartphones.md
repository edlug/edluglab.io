---
title: July 2019 Talk | Open Source Smartphones
author: Tai Kedzierski
layout: post
---

For July 2019, Thomas Kluyver will be giving us a rundown of the state of open source smartphones!

> ## Where's my open smartphone?
>
> When Apple launched the iPhone in 2007, phones completed a journey from appliances to pocket computers. Over three quarters of adults in the UK own a smartphone. But it's difficult to run free, open source applications and operating systems on your phone, or to get a phone with open hardware and firmware. How open are the big smartphone platforms? What are the various projects aiming to make our phones more open? And what are Purism doing with the $2 million they raised by crowdfunding for the Librem 5?

Open Tech Calendar: <https://opentechcalendar.co.uk/event/8611-wheres-my-open-smartphone>

Note that this meetup will take place at [Akva, Fountainbridge, Edinburgh](https://www.openstreetmap.org/search?query=Akva%2C%20Edinburgh#map=19/55.94318/-3.20833)

