---
title: January 2020 (talk) | Chocolatey - a Package Manager for Windows
author: Tai Kedzierski
date: 2019-09-19 12:09:39
layout: post
---

To kick off the new year Paul Broadwith will give us a talk on some
technology you'd probably not expect to feature at a LUG...

He'll be talking about the open source edition of [Chocolatey][choco], a
package manager for .... Windows !

> "Wait, EdLUG will talk about a Windows tool ??"

It is, more generally, an Open Source tool (Apache 2.0 license), which
also enables a new way of discovering and installing applications.
Introducing CLI package management to Windows users can be a helpful
way to ease them into the idea of using a terminal - and make the case
for Linux that much easier to present.

It runs on the command line, typically in PowerShell, and behaves
pretty much like package managers you know like APT, dnf/yum or npm.
In fact, it is more like 'yaourt' and the [Arch User Repository][aur] in that
the packages are community-curated. You can even install graphical
apps like Firefox and Filezilla with it.

So come along for a journey of discovery, Windows and Linux users alike!

<!-- We just need someone to do a talk on homebrew for Mac next :-p -->

[choco]: https://chocolatey.org/
[aur]: https://wiki.archlinux.org/index.php/Arch_User_Repository
