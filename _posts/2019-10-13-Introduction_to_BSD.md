---
title: March 2020 Talk - Introduction to BSD
author: Tai Kedzierski
date: 2019-10-13 18:51:44
layout: post
---

Laurie Ibbs will be talking to us about the Berkley Software Distribution, a direct descendent of the Bell Labs UNIX system and now most popularly known through the FreeBSD, NetBSD and OpenBSD projects. It is an Open Source, permissively-licensed operating system which often both competes with and complements Linux, primarily in the server space.

Laurie will be giving us an introduction to the system, along with highlighting the main differences that exist when comparing it to Linux.

19:00 - meet, upstairs section of Akva
19:15 - start talk
20:00 - closing notes, socialise, play ping pong

+++

If you have never been to one of these meetings, just know that it's friendly and informal - grab a pint or a soda, come ask questions, get help with open source software and Linux, and enjoy some geeky company!

There is often a Linux laptop on hand for a quick show and tell for anyone who is curious, and you can also bring your own laptop to troubleshoot Linux, or try Linux on your own laptop - without installing it.

