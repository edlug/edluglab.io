---
title: September 2019 Talk | State of Play - Gaming on Linux with Steam and Proton
author: Tai Kedzierski
date: 2019-09-01 16:56:45
layout: post
---

This month's talk will be on running games on Linux, using Steam, and the state of Proton's support for Windows games!

We'll be running the next meetup once again at Akva. It is booked for Tuesday 3rd of September at 7pm, for a tentative 7:15pm start of the talk - hopefully. There should be no sports on, though there is still a pub quiz on at 8pm.

We will be in the upper mezzanine, which is accessible either via the stairs, or via the lift by the street-side entrance behind the bar.

