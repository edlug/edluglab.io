---
title: November 2019 Talk | Jupyter Notebooks and Kubernetes
author: Tai Kedzierski
date: 2019-09-03 17:01:08
layout: post
---

This month Ian Stuart will be giving us an overview of Jupyter Notebooks, a solution for active web-based documents with running code in them, and how he deployed it on Kubernetes.

More details to follow.

We'll be meeting at Akva at 7pm, for a 7:15pm start, in the upper mezzanine.

