---
title: February 2020 (talk) | Introduction to Linux - Get the Best out of Your Computer
author: Tai Kedzierski
date: 2019-09-19 14:32:34
layout: post
---

Right in line with the back-to-uni crowd, we'll be doing an Introduction to Linux session for those who are curious about it but never knew where to start! We'll cover

* What it is
* Who uses it
* How to get it
* How to install it
* Introduce the concept of Live USB
* How to find and install software on it

After which, we'll do a quick showcase of some popular, beginner-friendly distros. You can bring your own laptop to check if Linux would run on it using a Live USB session !

**Who is this for?**

This is aimed at people who might touch programming code occasionally, or even regularly, from novice student all the way to seasoned professionals, so you might be of these groups:

* Users of statistical software like `R`, `SciPy` or `Pandas`
* Computer scientists
* Web developers
* Back-end developers and system administrators
* Other science disciplines like physics/astrophysics, chemistry/biochemistry, mathematics/statistics, pyschology, economics, data-oriented science in general

Anything that in your work requires you to crunch numbers, really! If you're using a computer for serious number crunching, or computer deployment, we'd like to show you what the Free Software and Open Source world has to offer - starting with a quick talk, and moving on to a hands-on event where you'll be able to try out popular Linux variants, suited to your needs.


We hope to see you there!
