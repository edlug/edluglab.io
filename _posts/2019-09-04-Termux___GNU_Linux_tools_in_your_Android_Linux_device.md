---
title: October 2019 Talk | Termux - GNU/Linux tools in your Android/Linux device
author: Tai Kedzierski
date: 2019-09-03 16:58:32
layout: post
---

This month we will be getting a talk from JB about Termux, a fantastic terminal emulator and GNU/Linux sub-environment for your Android device!

More details to follow, will keep you posted!

We'll be convening at Akva at 7pm for a 7:15pm start, in the upper mezzanine.

