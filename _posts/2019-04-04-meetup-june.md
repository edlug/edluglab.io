---
layout: post
title: June 2019 (talk) | Creating websites with Jekyll and GitLab
date: 2019-04-04 16:10
author: Tai Kedzierski
---

Tai has gradually been moving the EdLUG website to Gitlab's hosting service, with a static website written with markdown and built with Jekyll.

This talk will briefly go over the benefits for why it was done, and demonstrate how a basic website can be created using a the [ruby-based Jekyll](https://jekyllrb.com/) and [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/), building with GitLab's CI system.
