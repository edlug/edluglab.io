---
layout: post
title: May 2019 (talk) | Writing an Emulator to Run Ancient Software on a Modern Computer
date: 2019-04-04 16:00
author: Tai Kedzierski
---

Previously scheduled for May was my (Tai's) talk on the website, but our guest speaker has a very interesting topic to share - and will be leaving the country soon. So I've bumped mine to June.

Our guest speaker Rhys Rustad-Elliott will be talking to us about emulators, after his mini-demo during the last meetup!

> Writing an emulator that can run 30+ year old software on a modern computer is incredibly fun, and easier than you'd think. This talk will give an overview of how emulators work, and include demos of emulators for the Nintendo Entertainment System and Altair 8800, going over how they work at the source code level. No knowledge of computer hardware will be assumed.
