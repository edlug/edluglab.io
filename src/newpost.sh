#!/usr/bin/env bbrun

# Build this script using Bash Builder :
# https://github.com/taikedz/bash-builder

#%include std/out.sh
#%include std/askuser.sh
#%include std/varify.sh
#%include std/syntax-extensions.sh

$%function main(?author ?title) {
    cd "$(dirname "$0")"

    [[ -n "$author" ]] || author="$(askuser:ask "Your name")"
    [[ -n "$title" ]]  || title="$(askuser:ask "Post title")"

    nowdate="$(date +%F)"
    postfile=_posts/"$nowdate-$(varify:fil "$title").md"



    cat > "$postfile" <<EOPOST
---
title: $title
author: $author
date: $(date +"%F %T")
layout: post
---

(Write your text here)

EOPOST

    if [[ -n "${EDITOR:-}" ]] && askuser:confirm "Use $EDITOR to open $postfile ?"; then
        "$EDITOR" "$postfile"
    else
        out:info "Now edit $postfile in your preferred editor."
    fi
}

main "$@"
