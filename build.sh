#!/usr/bin/env bash

minima_last_version="2.5.0"
minima_version="$(gem list '^minima$' | grep -oP '[0-9]+\.[0-9]+\.[0-9]+')"

if [[ "$minima_version" != "$minima_last_version" ]]; then
    echo "WARNING - minima has updated ($minima_version). Check your _includes!"
fi

bundle exec jekyll serve -d public --watch
