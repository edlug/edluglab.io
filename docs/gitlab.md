# How to use Gitlab to update the website

This is a quick guide to using GitLab's online editor to update the EdLUG website. Anybody is welcome to submit content!

## Create an account

First you will need a free GitLab account.

<small>(GitLab is funded by their sales of premium accounts, and sales of services to customers who use GitLab in-house at their companies.)</small>

## Fork the website code

Once you have logged in with your GitLab account, you can visit the EdLUG website project at <https://gitlab.com/edlug/edlug.gitlab.io> and click the `Fork` button in the top right

![Fork button in gitlab](gitlab_forkbutton.png)

You will be asked for a namespace into which to fork the site - choose your name.

![Fork destination choice](gitlab_forkdest.png)

You will now have your own copy of the EdLUG website! It will be in your space, under something like `https://gitlab.com/YOUR_NAME/edlug.gitlab.io`

## Use the Web IDE editor

Navigate to the page you wish to edit - for example, click on the [`linux.md`](https://gitlab.com/taikedz/edlug.gitlab.io/blob/master/linux.md) file to update the About Linux page.

At the top right of the file contents that are displayed, choose `Web IDE`. This will open an online editor allowing you to edit the file. You can also edit other files in the same session.

![Online IDE](gitlab_editfile.png)

### Use Markdown

The text files all use a language called "[Markdown](https://daringfireball.net/projects/markdown/basics)." It's a very simple notation language which allows you to type raw text, which will then be turned into styled text. For example:

```
# Titles start with "#"

Regular text stays on its own line. `Literal text goes in backticks`
This line is on the same line as the other - use a blank line to make paragraphs.

* You can add
* Bullet points
* Anywhere!
```

Will look like this:


> # Titles start with "#"
> 
> Regular text stays on its own line. `Literal text goes in backticks`
> This line is on the same line as the other - use a blank line to make paragraphs.
> 
> * You can add
> * Bullet points
> * Anywhere!

You can find out more about the options in Markdown here <https://daringfireball.net/projects/markdown/basics>

### Save ("Commit") your changes

Once you have edited the files you wish to, you must "commit" them. This essentially means, saving all the changes amongst all the files in one go, with a message to describe the changes.

For programmers, this is useful beacuse a single change often involves changing multuiple files at the same time. When they come back to revisit the change, they want this fact to be tracked. Because this website is hosted from within a programming-oriented environment, we must follow the same practice.

![Commit button](gitlab_commitbutton.png) ![Commit message](gitlab_commitmessage.png)

1. Write a "commit message" - the first line should be under 50 characters
    * No need to make a lenghty description - just one that summarizes why the changes were needed
    * This is so the person looking back on it later can follow what happened
2. Choose the `Create a new branch and merge request` option
    * This is used so that you can send your changes to us!
3. Click the `Stage and commit button`

## Make a Merge Request

You will be taken to the merge request screen. This should have all the details it needs already, so you can scroll down and click `Submit merge request`

Do not delete your copy of `edlug.gitlab.io` - keep it until somebody has gotten back to you to confirm that they have merged your changes into the real site. (After that is done, you can delete your copy at `https://gitlab.com/YOUR_NAME/edlug.gitlab.io` by going to the settings gear in the bottom left menu, expanding the `Advanced` section, and scrolling down to the deletion option)

You are now done! Thank you for your contribution!
