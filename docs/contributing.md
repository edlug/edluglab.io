# Contributing changes

Anybody is welcome to update the website by sending a Merge Request or patch file!

If you need to email patches or changes, please send to <maintainer@edlug.org.uk>

All pages use [markdown](https://daringfireball.net/projects/markdown/basics) as it's very easy to use - no web page knowledge required!

### New blog posts

You can use the `newpost.sh` script to add a new file with the correct headers by simply running

```sh
bash newpost.sh "Jay Smith" "The title of my post"
```

This will create a new blog post file which you can then proceed to edit.

Alternatively, create a new blog post manually:

* Create a new file in `_posts/` with a file name comprising of a date in `YYYY-MM-DD` format, followed by a title with no spaces in it - for example `_posts/2019-03-05-some-title.md`
* Add a header starting and ending with `---` listing the title, author and layout type (which is always `post`)

```
---
title: The title of my post
author: Jay Smith
layout: post
---

```

Add a couple of empty lines after it, then start writing your blog post. See the other files in `_post/` for reference.

## Pull request / Merge request

If you are familiar with GitLab Merge Requests or Github Pull Requests, please fork this repository, make your changes, and make a pull/merge request onto the `master` branch.

If you are new to this workflow, check out the [Gitlab guide](gitlab.md).

If you are using a different hosted system than GitLab, please clone and push this repository to your preferred platform, make your changes, and mail the list, detailing the URL to your clonable repo, and the branch you wish to submit (can be `master`).

## Patch files

If you use git but you do not have any online git accounts, clone this repository, make your edits, then run:

```sh
git diff > edlug-site-update.patch
```

And mail the patch file to <maintainer@edlug.org.uk>. Remember to specify the commit your based your changes off of, so that the patch can be applied correctly.

## Online editor

Requires you to have a GitLab account. See the [GitLab online editing guide](gitlab.md) for a walkthrough with screenshots.

It boils down to:

* Create an account with Gitlab
* Fork this repository
* Use the web editor to make changes
* Submit a *Merge Request* for us to integrate, choosing the `master` branch as your target

## None of the above

Please send an email detailing any site corrections you would like to request to <maintainer@edlug.org.uk>.
