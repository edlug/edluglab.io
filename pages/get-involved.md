---
layout: page
title: Get involved!
permalink: /get-involved/
---

## Meetups and Events

Our events diary is published on the [Open Tech Calendar](https://opentechcalendar.co.uk/group/32-edinburgh-linux-users-group) and on [Meetup.com](https://www.meetup.com/Edinburgh-Linux-User-Group-EdLUG/). We meet up in a pub room once a month for talks and socials.

If you have never been to one of these meetings, just know that it's friendly and informal - grab a pint or a soda, come ask questions, get help with open source software and Linux, and enjoy some geeky company!

There is often a Linux laptop on hand for a quick show and tell for anyone who is curious, and you can also bring your own laptop to troubleshoot Linux, or try Linux on your own laptop - without installing it.

We look forward to seeing you!

## Talks

We try to have a talk for every other meetup, and more frequently if we can.

If you have a talk you would like to give that is related to Linux, Free and Open Source Software, or digital freedom and independence, we'd love to hear from you! Get in touch via the [mailing list](/mailing-list/), or email <maintainer@edlug.org.uk> -- or better yet, come see us in person!

If you're looking for inspiration, see our [list of previously suggested topics](https://gitlab.com/edlug/Meetings/wikis/Call-for-Talks) - where you can also add your own suggestions - and our [list of past talks](/past-talks/).

## Outreach

If you have an idea to help with outreach, either in growing the EdLUG group itself, or for promoting Free and Open Source Software adoption, do let us know. We would be happy to collaborate!

## Suggestions, ideas and coordination

You can view the current projects and efforts [on our GitLab issues page](https://gitlab.com/edlug/Meetings/issues)

If you would like to chip in with helping organise larger events for the group, please consider creating a [GitLab account](https://gitlab.com/users/sign_in#register-pane) and contributing directly! ([about the software we use](/pages/software.html))
