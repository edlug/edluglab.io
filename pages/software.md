---
author: Tai Kedzierski
layout: page
exclude: true
---

## About the external hosts we use

Some notes about what 3rd party hosts we use and why we use them.

### Open Tech Calendar

The [Open Tech Calendar](https://opentechcalendar.co.uk/) is a wiki-like events calendar and meetups directory, run and hosted by Edinburgh-based developer and meetup organiser [James Baster](https://www.jmbtechnology.co.uk/), and acts as a counterpart to the alternative pay-for sites that are often out of budget for many hobbyist groups. It also serves as a focus for technical meetups, as there are no other "noise" meetups to sift through to find the technical groups.

Most notably, its content is entirely [community-driven](https://opentechcalendar.co.uk/about), from the [back-end code](https://github.com/OpenACalendar/OpenACalendar-Web-Core) to the data itself. Anybody can list a group or a meetup for a group, and ensure their group's presence online. It also pointedly maintains an open data policy, allowing other sites to integrate with it freely and list its contents.

As a list, this would be:

* It is open source, and thus in-line with the general EdLUG ethos to favour open source platforms.
* It can be self-hosted, and all data can be transferred into a self-hosted instance easily should the need arise (we avoid cloud lock-in)
* The data is all-open (apart from basic user emails and passwords), meaning the content can be consumed by other platforms in any way they chose

#### Meetup.com

As of mid-2019, the Tai decided to create a Meetup.com profile for EdLUG, in order to reach people where they are. It is not built with Free Software or Open Source ideas in mind, but is very popular for organising events. By joining Meetup and raising our profile there, Tai hopes to draw more attention to the digital issues FOSS advocates believe in amongst those who may not yet be thinking about them.

### GitLab

The maintainer uses GitLab on the cloud-hosted GitLab.com to track activities as of 2019

* It is open source, and thus in-line with the general EdLUG ethos to favour open source platforms.
* It can be self-hosted, and all projects can be transferred into a self-hosted instance easily should the need arise (we avoid cloud lock-in)
* The company themselves have a generous free tier, which is funded by their [paying tiers](https://about.gitlab.com/pricing/)
* [Their privacy policy](https://about.gitlab.com/privacy/) sounds legit. <small><i>I am not a lawyer</i></small>
