---
layout: page
author: Tai Kedzierski
---

## Get Started

Do you want to give [Linux](/pages/linux.html) a whirl? Try one of the suggestions below. The following are simply suggestions from the maintainer, and are free to edit. They are not an endorsement for any particular purpose or aim.

> When trying Linux for the first time, you'll often see instructions to "flash/burn an ISO to a USB stick" - you can do this in a few clicks [with Etcher](https://www.balena.io/etcher/)
>
> And when you're done, you'll need to [reboot your computer in a way that it recognizes the USB](https://lifehacker.com/how-to-boot-from-a-usb-drive-or-cd-on-any-computer-5991848)

### Distros

Pick something. Or try them all. Download an "ISO" of the distro you want to try, and then "burn the ISO to a USB stick" ;-)

| [Ubuntu](https://www.ubuntu.com/#download)    | The most commonly installed Linux desktop distro. Friendly and polished, occasionally exposing its geek roots. There are lots of guides out there on websites and forums, so you're rarely "truly" lost.
| [Linux Mint](https://www.linuxmint.com/)      | Another very popular distro, based on Ubuntu. Ergo, lots of guides out there too.
| [Elementary OS](https://elementary.io/)       | Something familiar, something new. Based upon Ubuntu too. Aimed at regular home users, styled to be familiar to people coming from macOS.
| [Solus](https://getsol.us/home/)              | Something that you'll never need to "reinstall" to update\*. Keep that computer going forever. Aimed at regular home users.
| [Debian](https://www.debian.org/)             | If you want a little more geeky control, but not too much. Lots of *technical* guides out there.

Anything else and you probably know what you're doing, right? See all distros at [DistroWatch][distrowatch]!

\* This is what is known as a "rolling release". There's no "Solus 1" or "Solus 2" - it's just "Solus" and it keeps on updating over time.

### Blogs and sites for information

| [OMG Ubuntu](https://www.omgubuntu.co.uk/)    | A blog that often talks about fun aplications to use on Ubuntu - for new Ubuntu and Linux users
| [LifeHacker \| Open Source section](https://lifehacker.com/tag/open-source) | Geekier site that lists additional apps for everything from home projects to professional needs
| [Switching.social](https://switching.social)] | If you are getting fed up with software that nags you to sell your soul, checkout Switching.social's list of ehtical software and site alternatives
| [Destination Linux \| Podcast](https://destinationlinux.org/) | A podcast by Linux enthusiasts, to get your feet wet and give you ideas

And some extras for the extra mile:

| [DistroWatch][distrowatch]   | A long list and tracker of as many of the distros out there as they can locate
| [What is (truly) Free Software](https://www.fsf.org/about/what-is-free-software) | "... and why is it important to society." We probably wouldn't have Linux and a thriving Open Source movement without the efforts of its more vocal side: the *Free Software*\* movement.
| [The Linux Foundation](https://www.linuxfoundation.org/) | Linux, as a business technology - for those who want to make a career of it

<small>\*\* The mantra that <i><a href="https://www.gnu.org/philosophy/free-sw.en.html">"Free Software is a matter of liberty, not price"</a></i> comes from this movement.</small>

[distrowatch]: https://www.distrowatch.com/
