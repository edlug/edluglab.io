---
layout: page
author: Tai Kedzierski
---

## What is Linux?

Linux is a Free operating system\* - free both in the sense of *"free lunch"* and *"freedom."*

It does not spy on you. You are in control.

It is open source - you are entitled to know what is running on your computer, and to keep it running, long after the creator moves on.

It does not slow your computer down to encourage you  to buy a new one (it will happily bring that 5 year old laptop back to top speed - and make it good for another 5 more years -- or even longer!)

It runs on nearly any consumer desktop or laptop computer.

It can run without being installed. [Try it out](/pages/get-started.html).

It is community-centred. People help eachother out.

### Operating systems / Distros

\* For those who seek precision: Linux itself is the kernel, the core, of an operating system, which is frequently bundled with the GNU userland tools. In this context, it is sometimes referred to as "GNU/Linux". The GNU tools are a de-facto standard of most general purpose Linux-based operating systems, and allow users of all kinds to then write software for their own computers.

You may have heard of such Linux-related systems called "Ubuntu", "Debian", "Fedora" and "Arch." These are actual operating systems - a full, end product that combine a Linux kernel, GNU tools, and various user interfaces, applications, and polished ease-of-use features. They are "distributed" as discrete, discernable operating systems in their own right - and are thus called "distros."

Other Linux operating systems include Android and ChromeOS, but are non-GNU, and typically as a result do not promote user freedom.
