---
layout: page
exclude: true
permalink: /maintainers/
---

# Maintainers & Organisers

These fine people are responsible variously for maintaining the mailing list, website and other online assets, as well as organising meetups and ensuring the [Code of Conduct](/coc/) is observed.

If you have a general query, please prefer mailing <maintainer@edlug.org.uk>.

If you have been harassed or received unwanted attention/communication from a member of our community, in the context of EdLUG events and communications, please let us know as soon as possible.

<style>
li p img {
    max-width: 128px;
    max-height: 128px;
}
</style>

* ![Tai](/assets/profile/tai.jpg) Tai Kedzierski, <dch.tai@gmail.com>, <maintainer@edlug.org.uk>

* ![Thomas](/assets/profile/thomas.jpg) Thomas Kluyver, <thomas@kluyver.me.uk>
