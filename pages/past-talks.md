---
layout: page
title: Past talks
permalink: /past-talks/
---

- 2022-02-01: A crash course in embedded linux hardware, *Craig Burden*
- 2021-12-07: On day $N of Christmas, FOSS gave to me... *Multiple speakers*
- 2021-11-02: State of Play for Linux Gaming, *Shehraz Ditta*
- 2021-09-29: Audio on Linux, *Multiple speakers* (Joint event with Belfast & Portsmouth LUGs)
- 2021-09-07: GitLab as an OSS solution for organizing work & life, *Nicola*
- 2021-07-06: Penetration-testing webapps, *Panda*
- 2021-06-01: D-Bus, *Thomas Kluyver*
- 2021-05-04: Securing Wordpress, *Panda*
- 2021-04-06: Markdown for Beginners, *Tai Kedzierski*
- 2021-03-02: GlusterFS: expandable network storage solution, *Keith*
- 2021-02-02: Subdomain takeover, *Panda*
- 2020-12-01: Quiz night!
- 2020-11-03: Quiz, *Marion* & Introduction to Virtualbox, *Tai Kedzierski*
- 2020-10-06: Computer Chronicles and the Wayback Machine, *Shehraz Ditta*
- 2020-09-01: At your command - why & how commands help you, *Tai Kedzierski*
- 2020-07-07: Taking the back off the terminal, *Thomas Kluvyer*
- 2020-06-02: Linux Rootkits, *Gordon Gray*
- 2020-05-05: Where's the water? Trying to bring FOSS to a laid back community, *Jonathan Riddell*
- 2020-04-07: Everyone loves a sausage: running your own public cloud platform, *Nick Jones*
- 2020-03-03: Introduction to BSD, *Laurie Ibbs*
- 2020-02-04: Introduction to Linux - a distro showcase session, *Tai Kedzierski*
- 2020-01-07: Social meeting
- 2019-11-05: Jupyter Notebooks and Kubernetes, *Ian Stuart*
- 2019-10-01: Termux - Unlock the power of the terminal on Android, *JB*
- 2019-09-03: State of play for gaming on Linux, *Shehraz Ditta*
- 2019-07-02: Where's my open source smartphone, *Thomas Kluyver*
- 2019-06-04: Creating a website with Jekyll & GitLab.io, *Tai Kedzierski*
- 2019-05-07: Writing an emulator to run ancient software on a modern computer, *Rhys Rustad-Elliot*
