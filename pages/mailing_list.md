---
layout: page
title: Mailing List
permalink: /mailing-list/
---

The heart of EdLUG is its mailing list - it serves as the main means of communication for members. Membership of the group is pretty much *de facto* defined by membership of the mailing list, as there is no other way we track association with us.

* Mail us: <edlug@lists.edlug.org.uk>
* [Subscribe to the mailing list](https://mailman.lug.org.uk/mailman/listinfo/edlug)
* [View mail archives](http://lists.edlug.org.uk/pipermail/edlug/) for lug.org.uk-hosted mail
    * [View old archives](http://edlug.org.uk/mail_archive/) (until November 2013, for old mail system)

If you need to get in touch with the maintainer directly, please email <maintainer@edlug.org.uk>

Over time the list has developed its own etiquette, the guidelines of which are reproduced below.

It runs parallel to our [Code of Conduct](/coc/) which applies both to the mailing list, and to our meetups, and any events organised under the EdLUG name.

> ### EdLUG Mailing List Guidelines
> 
> *1 - Please send messages from the address you subscribed with*
>
> If your `From:` address is not on the subscription list, your email will be held and/or bounced by the system.
>
> If you have something you wish to send to the maintainer only, please use <maintainer@edlug.org.uk>
> 
> *2 - Please be nice, no flame wars*
> 
> People of all ages read this list. Please don't send anything you would not want your own children to read.
> 
> Please keep primarily to the topics of EdLUG, Linux, Open Source, or digital rights.
> 
> *3 - I don't live in Edinburgh, can I still join?*
> 
> Yes - although this is the Edinburgh Linux Users Group we welcome anyone with an interest in Linux no matter their geographical location.
> 
> *4 - I'm not a member of EdLUG can I still join the mailing list?*
> 
> Most certainly - by doing so, you will effectively become a member!
> 
> *5 - Can I advertize on this list?*
> 
> In principle no, but...
> 
> * Use common sense
> * If you have some computer equipment you're giving away in the Edinburgh/Midlothian area then please let the list know.
> * If you would like to give a talk, tutorial, demonstration, BBQ etc. please let us know
>
> Job advertising:
>
> * As a general rule of thumb if you're not an agency, send in a cautious plain text short advert. Please prepend the subject with `[JOBS]`.
> * If you are wishing to advertise a job in your capacity as a recruiter, and you are not involved with GNU/Linux *per se*, then for the moment email them (in plain text) to <maintainer@edlug.org.uk>.
> * If you're looking for a job and you're a regular member of EdLUG please feel free to remind us that you are available, but keep it short and don't send your CV to the list - perhaps a link for example.
> 
> *6 - Do you have an archive?*
> 
> Yes. There is an archive for the old mailing list up to Q2 2013, at which point we switched to a different mailing list system hosted by Lug.org.uk. If you would like your address removed from our archive, please contact the maintainer.
>
> The current archive is now hosted at <http://lists.edlug.org.uk/pipermail/edlug/>
> 
> *7 - Who runs the list?*
>
> [![Don't got it alone - join UK LUGs](/assets/logo_lug.gif)](https://lug.org.uk)
>
> The list is hosted and run from <https://mailman.lug.org.uk/mailman/listinfo/>, the general site for UK LUGs.
> 
> It is maintained by the current group convenors, listed on the [Maintainers page](/maintainers/)

Amended:

* Faye Gibbins, Sep 2nd 2002
* Jan Goulding, Nov 22nd 2014
* Tai Kedzierski, Feb 18th 2019


