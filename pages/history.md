---
layout: page
title: History
permalink: /history/
---

EdLUG originated within the UNIX Group at Edinburgh University Computing Services. In early 1998, use of Linux was on the rise and either Keith Farvis (the group's manager) or Rob McCron came up with the idea of starting a local users' group.

Rob took the lead on organising that first meeting, which was held on Wednesday April 1st 1998 in the Cask and Barrel on Broughton Street. The idea was that the UNIX Group could bring together a community of interested people, provide some support, and the group would gradually find its own character and structure. 19 people showed up, although they weren't necessarily all there at the same time. Rob took minutes which are still available.

The second meeting was on Thursday May 5th 1998. 24 people showed up. This meeting agreed on the name EdLUG and came up with the format of a formal element (talk/expert panel) followed by pub chat. At this point Alistair Murray took the Folder Of Office from Rob and the group began in earnest. There are rather token minutes from this meeting.

Fairly early on the meetings shifted to the Holyrood Tavern. University of Edinburgh helped out with web hosting for a while and mailing list for rather longer. 

Under Jan Goulding's maintainership, the meetings moved to the Southsider in the Old Town, and our online presence grew to include a website graciously hosted by Gladserv, and the mailing list moved to Lug.org.uk.

In 2019 the LUG found it needed to move on from the Southsider to Akva, and simplify its online presence. This is now a static website on Gitlab.io, while the mailing list is still hosted by Lug.org.uk

The coronavirus pandemic in 2020 put a stop to physical meetups, and EdLUG moved online, continuing with monthly meetings. [See the events page](/get-involved/) for more details.
