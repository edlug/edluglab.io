---
layout: page
author: Tai Kedzierski
---

# Site map

* [Home](/)
* [Code of conduct](/coc/)
* [Mailing list](/mailing-list/)
    * [Current archive](http://lists.edlug.org.uk/pipermail/edlug/)
    * [Old archive](http://edlug.org.uk/mail_archive/)
* [Get involved](/get-involved/)
* [Software we use](/pages/software.html)
* [History of EdLUG](/history/)

About Linux

* [Linux](/pages/linux.html)
* [Get started](/pages/get-started.html)
