# Welcome to EdLUG

This group and repository has been created for the Edinburgh Linux User Group's website and further assets.

Visit our homepage at <https://edlug.gitlab.io/>!

## Contributing

See [the contributing page](docs/contributing.md)

## How to build the site locally

### Setup

You need Ruby and Jekyll, and GCC for compiling some Ruby dependencies.

Run the [`./setup.sh`](setup.sh) script to install ruby, Jekyll, and required gems to get started.

### Build

Run the `./build.sh` script to use Jekyll to rebuild the site and view it in demo-mode at <http://localhost:4000>

Press `Ctrl + C` to stop serving
