---
# Feel free to add content to this file, or add corrections!

layout: home
---

![EdLUG](/assets/logo_edlug.png){: style="float:right;""}

EdLUG is a group of like-minded people from the Edinburgh area who are interested in [Linux](/pages/linux.html), Open Source software and hardware and related concepts. Our members are from all walks of life and from across the whole skill and knowledge range, from enthusiasts and professionals to hobbyists and newcomers. Membership of EdLUG is free and open to anyone.

We meet online on the first Tuesday of each month\* from 7:00pm onwards. Talks aim to start at 7:15pm.

<hr />

To become a member, simply [join the mailing list](/mailing-list/)

To meet in person, [check out events page](/get-involved/)

To start using Linux, check out our [Get Started](/pages/get-started.html) notes.

\* Except:

* August (because of the festival)
* December (which is the Christmas dinner outing)
* If the first Tuesday lands on the 1st or 2nd of January, it rolls over to the following week instead.

<hr />

**Code of Conduct**

EdLUG is dedicated to providing a harassment-free community for everyone. We do not tolerate harassment of or by community members in any form. Our full Code of Conduct [can be found here](/coc/)

<hr />
